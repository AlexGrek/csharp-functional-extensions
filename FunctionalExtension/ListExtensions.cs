﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionalExtension
{
    public static class ListExtensions
    {
        public static IEnumerable<K> Map<T, K>(this IEnumerable<T> list, Func<T, K> func)
        {
            var innerList = new List<K>();
            foreach (var element in list)
            {
                innerList.Add(func(element));
            }
            return innerList as IEnumerable<K>;
        }

        public static void Iter<T>(this IEnumerable<T> list, Action<T> func)
        {
            foreach (var element in list)
            {
                func(element);
            }
        }

        public static IEnumerable<K> Mapi<T, K>(this IEnumerable<T> list, Func<T, int, K> func)
        {
            var innerList = new List<K>();
            int i = 0;
            foreach (var element in list)
            {
                innerList.Add(func(element, i));
                i++;
            }
            return innerList as IEnumerable<K>;
        }

        public static void Iteri<T>(this IEnumerable<T> list, Action<T, int> func)
        {
            int i = 0;
            foreach (var element in list)
            {
                func(element, i);
                i++;
            }
        }
    }
}
