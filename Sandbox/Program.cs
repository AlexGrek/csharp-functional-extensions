﻿using FunctionalExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[] { 1, 2, 3 };
            var done = a.Mapi(((x, i) => x * i));
            done.Iter(Console.WriteLine);
        }
    }
}
